import { makeAutoObservable } from 'mobx'

class UserStore {
  constructor () {
    makeAutoObservable(this)
  }

  users = []

  setUsers (usersArr) {
    this.users = usersArr
  }
}

export const UsersState = new UserStore()

