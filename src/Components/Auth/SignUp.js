import React, { useState } from 'react'

const SignUp = ({ setCurrentAuthComponent }) => {
    const [email, setEmail] = useState('')

    const handleChangeEmail = (event) => {
        setEmail(event.target.value)
    }

    const handleChangeComponent = () => {
        setCurrentAuthComponent('SIGN_IN')
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', width: '30%' }}>
            <h2>Sign Up</h2>
            <input placeholder='Email' value={email} onChange={handleChangeEmail}/>
            <input placeholder='Password' type='password'/>
            <input placeholder='Repeat password' type='password'/>
            <button>Sign Up</button>
            <button onClick={handleChangeComponent}>Already have an account? Sign In!</button>
        </div>
    )
}

export default SignUp

