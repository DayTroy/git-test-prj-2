import React from 'react'
import classes from './helloWorldComponent.module.css'
const HelloWorldComponent = () => {
  return (
        <div>
            Hello World
            <div className={classes.block}/>
        </div>
  )
}
export default HelloWorldComponent
