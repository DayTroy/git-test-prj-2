import {observer} from 'mobx-react-lite';
import styles from './navigation.module.sass';
import {Link} from 'react-router-dom';
import logo from '../../source/images/navigation/logo.svg';
import avatar from '../../source/images/navigation/avatar.svg';
import dropIcon from '../../source/images/navigation/drop-icon.svg';

const Navigation = observer(() => {
    return (
        <div className={styles.navigation}>
            <div className={styles.navigation__links}>
                <Link to = {'/'} className={styles.navigation__logotype}>
                    <img src={logo} alt= "EverGaming logotype"/>
                </Link>
                <div className={styles.navigation__links_item}>
                    <Link className = {styles.navigation__links_link} to={'/tournament'}>Турниры</Link>
                    <Link className = {styles.navigation__links_link} to={'#'}>События</Link>
                    <Link className = {styles.navigation__links_link} to={'/commands'}>Команды</Link>
                    <Link className = {styles.navigation__links_link} to={'#'}>Статьи</Link>
                    <Link className = {styles.navigation__links_link} to={'#'}>Магазин</Link>
                    <Link className = {styles.navigation__links_link} to={'#'}>Кошелек</Link>
                </div>
            </div>
            <div className={styles.navigation__user}>
                <div className={styles.navigation__account}>
                    <div className={styles.navigation__avatar}>
                        <img src = {avatar} alt = "User photo"/>
                    </div>
                    <div className={styles.navigation__dropIcon}>
                        <img src={dropIcon} alt = {'bird'}></img>
                    </div>
                    <div className={styles.navigation__hamburger}>
                        <div className={styles.navigation__hamburger_item}/>
                        <div className={styles.navigation__hamburger_item}/>
                        <div className={styles.navigation__hamburger_item}/>
                    </div>
                </div>
            </div>
        </div>
    )
})
export default Navigation;