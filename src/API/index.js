import axios from 'axios'
export const API_GATEWAY = 'https://jsonplaceholder.typicode.com'
export const apiRoot = {
  getUsers: () => axios.get(`${API_GATEWAY}/users`)
}
